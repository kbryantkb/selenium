#each feature contains one feature
# feature files use Gherkin language - business language
Feature: test the login functionality of SDET University

	# A feature may have given diferent specific scenarios
	#Scenarios use Given-When-Then structure

	Scenario Outline: the user should be able to login
	Given user is on the login page
	When user enters username <username>
	And user enters password <password>
	And user clicks login
	Then user gets confirmation
	
	Examples: 
| username | password |
| tim@testemail.com | trpass |
| rw@testmail.com | rwpass |
| jv@testmail.com | jvpass |