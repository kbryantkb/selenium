package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	private WebDriver driver;

	public LoginPage(WebDriver drive) {
		super();
		this.driver = drive;
	}
	
	public void setUserName(String username) {
		driver.findElement(By.id("MainContent_txtUserName")).sendKeys(username);
	}
	
	public void setPassword(String password) {
		driver.findElement(By.id("MainContent_txtPassword")).sendKeys(password);
	}

	public void clickSubmit() {
		driver.findElement(By.id("MainContent_btnLogin")).click();
	}
}
