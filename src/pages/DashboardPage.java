package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage {
	
	private WebDriver driver;

	public DashboardPage(WebDriver driver) {
		super();
		this.driver = driver;
	}
	
	public void changePassword() {
		driver.findElement(By.linkText("Change password")).click();
	}
	
	public String getConfirmation() {
		return driver.findElement(By.id("conf_message")).getText();
	}
	
	
}
