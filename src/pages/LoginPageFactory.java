package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageFactory {
	
	//properties
	private WebDriver driver;
	
	@FindBy(id="MainContent_txtUserName")
	WebElement usernameField;
	
	@FindBy(id="MainContent_txtPassword")
	WebElement passwordField;
	
	@FindBy(id="MainContent_btnLogin")
	WebElement submitButton;

	//constructor
	public LoginPageFactory(WebDriver drive) {
		super();
		this.driver = drive;
		PageFactory.initElements(driver, this);
	}
	
	//methods
	
	public void login(String username, String password) {
		setUserName(username);
		setPassword(password);
		clickSubmit();
	}
	
	public void setUserName(String username) {
		usernameField.sendKeys(username);
	}
	
	public void setPassword(String password) {
		passwordField.sendKeys(password);
	}

	public void clickSubmit() {
		submitButton.click();
	}

}
