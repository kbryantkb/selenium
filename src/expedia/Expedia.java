package expedia;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.DriverFactory;

public class Expedia {
	
	WebDriver driver; 
	WebElement hotelTab, destination, checkIn, checkOut, travelers, addAdults, close, search, fourStars;
	int i, numOfAdults;
	String pageTitle;
	
	@Test
	public void hotelReservation() {
		
		numOfAdults = 5;
		
		defineElements();
		// 1. Search
		hotelTab.click();
		destination.sendKeys("New York, NY");
		checkIn.sendKeys("10/17/2019");
		checkOut.clear();
		checkOut.sendKeys("10/24/2019");
		travelers.click();
		for(i = 2; i < numOfAdults; i++) {
			addAdults.click();
		}
		close.click();
		search.click();
		//fourStars.click();
		// 2. Modify the search results page,  give criteria
		
		// 3. Analyze the results and make our selection
		
		// 4. Book reservation
		
		// 5. Fill out contact / billing
		
		// .6 Get confirmation
		
		
		
	}
	
	@DataProvider
	public void getData() {
		
	}
	
	public void defineElements() {
		hotelTab = driver.findElement(By.id("tab-hotel-tab-hp"));
		destination = driver.findElement(By.id("hotel-destination-hp-hotel"));
		checkIn = driver.findElement(By.id("hotel-checkin-hp-hotel"));
		checkOut = driver.findElement(By.id("hotel-checkout-hp-hotel"));
		travelers = driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-hotel\"]/div/ul/li/button"));
		addAdults = driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-hotel\"]/div/ul/li/div/div/div[1]/div[2]/div[4]/button"));
		close = driver.findElement(By.xpath("//*[@id=\"traveler-selector-hp-hotel\"]/div/ul/li/div/footer/div/div[2]/button"));
		search = driver.findElement(By.xpath("//*[@id=\"gcw-hotel-form-hp-hotel\"]/div[10]/label/button"));
		//fourStars = driver.findElement(By.id("star4"));
	}
	
	@BeforeMethod
	public void setUp() {
		driver = DriverFactory.open("chrome");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.expedia.com");
	}
	
	@AfterMethod
	public void tearDown() {
		//driver.quit();
	}
	
	

}
