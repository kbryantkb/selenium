package SeleniumTut;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;
import utilities.DriverFactory;

public class EnhancedTutTest {
	
	// grab confirmation text after form submit
	
	public static String getConfirm(WebDriver drive) {
		WebElement confirmation = drive.findElement(By.id("MainContent_lblTransactionResult"));
		String output = confirmation.getText();
		return output;
	}

	public static void main(String[] args) {
		
		//setting test variables
		
		/*String name = "Keith";
		String email = "kbryant@fakemail.com";
		String password = "ak47ak48";
		String country = "Belgium";
		String phoneNumber = "555-555-5555";
		String gender = "female";
		boolean weeklyEmail = true;
		boolean monthlyEmail = false;
		boolean occasionalUpdates = false;*/
		
		//instantiating UserClass objects with test data and loading them into an array
		
		UserClass keith = new UserClass("Keith","kbryant@fakemail.com","ak47ak48", "Belgium", "555-555-5555", "male", true, false, true );
		UserClass ricky = new UserClass("ricky","ricky@fakemail.com","rickrick1", "United States", "444-444-4444", "male", true, false, false);
		UserClass alec = new UserClass("alec","alec@fakemail.com","alec1", "Croatia", "333-333-3333", "male", false, true, false );
		UserClass userArray[] = new UserClass[] {keith, ricky, alec};
		
		//setting WebDriver variable
		
		WebDriver driver; 
		
		//looping through all users in userArray
		
		int i;
		for (i = 0; i < userArray.length; i++) {
	
			//initializing WebDriver 
			
			driver = DriverFactory.open("chrome");

			// open sdet sign in page
			
			driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
			
			//navigate to create account page
			
			driver.findElement(By.xpath("//*[@id='ctl01']/div[3]/div[2]/div/div[2]/a")).click();
			
			//setting web element variables
			
			WebElement nameField = driver.findElement(By.id("MainContent_txtFirstName"));
			WebElement emailField = driver.findElement(By.id("MainContent_txtEmail"));
			WebElement phoneField = driver.findElement(By.id("MainContent_txtHomePhone"));
			WebElement passwordField = driver.findElement(By.id("MainContent_txtPassword"));
			WebElement verifyPasswordField = driver.findElement(By.id("MainContent_txtVerifyPassword"));
			WebElement countryField = driver.findElement(By.id("MainContent_menuCountry"));
			WebElement maleButton = driver.findElement(By.id("MainContent_Male"));
			WebElement femaleButton = driver.findElement(By.id("MainContent_Female"));
			WebElement weeklyCheckbox = driver.findElement(By.id("MainContent_checkWeeklyEmail"));
			WebElement monthlyCheckbox = driver.findElement(By.id("MainContent_checkMonthlyEmail"));
			WebElement occasionalCheckbox = driver.findElement(By.id("MainContent_checkUpdates"));
			WebElement submitButton = driver.findElement(By.id("MainContent_btnSubmit"));
			
			//fill out form
			
				//enter name, email and phone number
			
				nameField.sendKeys(userArray[i].getName());
				emailField.sendKeys(userArray[i].getEmail());
				phoneField.sendKeys(userArray[i].getPhoneNumber());
				
				//select gender
				
				if(userArray[i].getGender().equals("male")){
					maleButton.click();
				}
				else {
					femaleButton.click();
				}
				
				//enter password 
				passwordField.sendKeys(userArray[i].getPassword());
				verifyPasswordField.sendKeys(userArray[i].getPassword());
				
				//select country
				
				new Select(countryField).selectByVisibleText(userArray[i].getCountry());
				
				//checkbox logic
				
					//weekly checkbox
					
					if(userArray[i].isWeeklyEmail()) {
						if(!weeklyCheckbox.isSelected()) {
							weeklyCheckbox.click();
						}
					}
					else {
						if(weeklyCheckbox.isSelected()) {
							weeklyCheckbox.click();
						}
					}
					
					//monthly checkbox
					
					if(userArray[i].isMonthlyEmail()) {
						if(!monthlyCheckbox.isSelected()) {
							monthlyCheckbox.click();
						}
					}
					else {
						if(monthlyCheckbox.isSelected()) {
							monthlyCheckbox.click();
						}
					}
					
					//occasional checkbox
					
					if(userArray[i].isOccasionalUpdates()) {
						if(!occasionalCheckbox.isSelected()) {
							occasionalCheckbox.click();
						}
					}
					else {
						if(occasionalCheckbox.isSelected()) {
							occasionalCheckbox.click();
						}
					}
					
				//end checkbox logic
				
			//end form entry
			
			//submit
			
			submitButton.click();
			
			//print confirmation to console or error message
			
			String expectedResults = "Customer information added successfully";
			
			if(getConfirm(driver).equals(expectedResults)) {
				System.out.println(getConfirm(driver) + " : TEST PASSED " + userArray[i].getName());
			}
			else {
				System.out.println("TEST FAILED");
			}
			
			//close the browser
			
			//driver.close();
		}
		
	}

}
