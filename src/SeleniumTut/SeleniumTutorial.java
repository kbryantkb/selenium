package SeleniumTut;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTutorial {
	
	static String printAccountName(WebDriver drive) {
		String results = drive.findElement(By.cssSelector("a[title=\"Keith Bryant\"] > div")).getText();
		return "Account Name: " + results;
	}
	
	static String printPageTitle(WebDriver drive) {
		String results = drive.getTitle();
		return "Page Title: " + results;
	}
	
	public static void main(String args[]) {
		
		// 1. open the web browser
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver");
		WebDriver driver = new ChromeDriver();
		// 2. Navigate to the web application
		// which ever web application in use *here*
		driver.get("https://www.facebook.com");
		driver.findElement(By.id("email")).sendKeys("keithbrotherhood@yahoo.com");
		driver.findElement(By.id("pass")).sendKeys("ak47ak48");
		driver.findElement(By.id("u_0_2")).click();
		
		//perform test
		
		//String results = driver.findElement(By.cssSelector("a[title=\"Keith Bryant\"] > div")).getText();
		
		System.out.println(printPageTitle(driver));
		System.out.println(printAccountName(driver));
		
		//close browser
		//driver.close();
		
		
	}
}


