package SeleniumTut;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumSignInTut {
	
	public static String returnConfirm(WebDriver drive) {
		String output = drive.findElement(By.id("MainContent_lblTransactionResult")).getText();
		return output;
	}

	public static void main(String[] args) {
		// set up firefox webdriver
		System.setProperty("webdriver.firefox.bin", "/Applications/FirefoxCurrent.app/Contents/MacOS/firefox-bin");
		
		System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/geckodriver");
		
		WebDriver foxDriver = new FirefoxDriver();
		
		// navigate open sdet sign in page
		
		foxDriver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
		
		//navigate to create account page
		
		foxDriver.findElement(By.xpath("//*[@id='ctl01']/div[3]/div[2]/div/div[2]/a")).click();
		
		//fill out form
		
		foxDriver.findElement(By.id("MainContent_txtFirstName")).sendKeys("Keith");
		foxDriver.findElement(By.id("MainContent_txtEmail")).sendKeys("kbryant@fakemail.com");
		foxDriver.findElement(By.id("MainContent_txtHomePhone")).sendKeys("555-555-5555");
		foxDriver.findElement(By.id("MainContent_Male")).click();
		foxDriver.findElement(By.id("MainContent_txtPassword")).sendKeys("ak47ak48");
		foxDriver.findElement(By.id("MainContent_txtVerifyPassword")).sendKeys("ak47ak48");
		
		new Select(foxDriver.findElement(By.id("MainContent_menuCountry"))).selectByVisibleText("Belgium");
		
		foxDriver.findElement(By.id("MainContent_checkWeeklyEmail")).click();
		foxDriver.findElement(By.id("MainContent_btnSubmit")).click();
		
		//get confirmation 
		
		System.out.println(returnConfirm(foxDriver));
		
		//close the browser
		
		foxDriver.close();

	}

}
