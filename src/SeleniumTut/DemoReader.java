package SeleniumTut;

//import java.util.List;
import utilities.Excel;

//import utilities.CSV;

public class DemoReader {
	
	//public static void readCSV() {
	//	String userFile = "/Users/keithbryant/Documents/synovustestlinks.csv";
	//	List <String[]> records = CSV.get(userFile);
	//	for (String[] record : records) {
		
			// use if statement only if first row are headers
	
	//		if(records.indexOf(record) >= 1) {
				/*for (String field: record) {
						System.out.println(field);
				}*/
	//			System.out.println(record[1]);
	//		}
	//		}
	//	}
	
	public static void readXLS() {
		String excelFile = "/Users/keithbryant/Documents/UserLogin.xls";
		String[][] data = Excel.get(excelFile);
		for (String[] row : data) {
			System.out.println("");
			System.out.println("NEW RECORD");
			for (String field : row) {
				System.out.println(field);
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			//readCSV();
		readXLS();
	}

}
