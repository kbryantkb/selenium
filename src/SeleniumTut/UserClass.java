package SeleniumTut;

public class UserClass {

	private String name, email, password, country, phoneNumber, gender;
	private boolean weeklyEmail, monthlyEmail, occasionalUpdates;
	
	public UserClass(String name, String email, String password, String country, String phoneNumber, String gender, boolean weeklyEmail,
			boolean monthlyEmail, boolean occasionalUpdates) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.country = country;
		this.phoneNumber = phoneNumber;
		this.gender = gender;
		this.weeklyEmail = weeklyEmail;
		this.monthlyEmail = monthlyEmail;
		this.occasionalUpdates = occasionalUpdates;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public boolean isWeeklyEmail() {
		return weeklyEmail;
	}

	public void setWeeklyEmail(boolean weeklyEmail) {
		this.weeklyEmail = weeklyEmail;
	}

	public boolean isMonthlyEmail() {
		return monthlyEmail;
	}

	public void setMonthlyEmail(boolean monthlyEmail) {
		this.monthlyEmail = monthlyEmail;
	}

	public boolean isOccasionalUpdates() {
		return occasionalUpdates;
	}

	public void setOccasionalUpdates(boolean occasionalUpdates) {
		this.occasionalUpdates = occasionalUpdates;
	}
	
}
