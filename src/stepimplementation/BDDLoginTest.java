package stepimplementation;

import org.junit.After;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import utilities.DriverFactory;

public class BDDLoginTest {

	WebDriver driver;
	
	//Scenario: the user should be able to login with correct username and correct password
	
	@Given("^user is on the login page$")
	public void user_is_on_the_login_page(){
		System.out.println("User is on the login page");
		driver = DriverFactory.open("chrome");
		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
	}
	
	/*@When("^user enter correct username and password$")
	public void user_enter_correct_username_and_password() {
		System.out.println("user enters username and password");
		driver.findElement(By.id("MainContent_txtUserName")).sendKeys("tim@testemail.com");
		driver.findElement(By.id("MainContent_txtPassword")).sendKeys("trpass");
		driver.findElement(By.id("MainContent_btnLogin")).click();
	}*/
	
	@When("^user enters username (.*)$")
	public void user_enters_username(String username) {
		System.out.println("TESTING: " + username);
		driver.findElement(By.id("MainContent_txtUserName")).sendKeys(username);
	}
	
	@And("^user enters password (.*)$")
	public void user_enters_password(String password) {
	driver.findElement(By.id("MainContent_txtPassword")).sendKeys(password);
	}
	
	@And("^user clicks login$")
	public void user_clicks_login() {
		driver.findElement(By.id("MainContent_btnLogin")).click();
	}
	
	@Then("^user gets confirmation$")
	public void user_gets_confirmation() {
		System.out.println("user gets confirmation");
		Assert.assertTrue(driver.findElement(By.id("conf_message")).getText().contains("success"));
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
	
	
}
