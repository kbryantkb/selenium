package smoketest;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.DriverFactory;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

public class LoginPresentTestNG {
	
	private WebDriver driver;
	
	@Test
	public void loginElementsPresentTest() {
		System.out.println("Running Test");
		
		String webURL = "http://sdettraining.com/trguitransactions/AccountManagement.aspx";
		driver.get(webURL);
		
		boolean loginEmailText = driver.findElement(By.id("MainContent_txtUserName")).isDisplayed();
		boolean passwordBox = driver.findElement(By.id("MainContent_txtPassword")).isDisplayed();
		
		Assert.assertTrue(loginEmailText);
		Assert.assertTrue(passwordBox);
	}
	
	@BeforeMethod
	public void setUp() {
		driver = DriverFactory.open("chrome");
		System.out.println("Starting Test");
	}
	
	@AfterMethod
	public void tearDown() {
		System.out.println("Closing Test");
		driver.close();
	}

}
