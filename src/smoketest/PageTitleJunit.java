package smoketest;

import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import utilities.DriverFactory;

public class PageTitleJunit {
	
	WebDriver driver;
	String webURL = "http://sdettraining.com/trguitransactions/AccountManagement.aspx";
	
	@Test
	public void pageTitleTest() {
		System.out.println("running the test");
		driver.get(webURL);
		String actualTitle = driver.getTitle();
		String expectedTitle = "SDET Training | Account Management";
		
		assertEquals(actualTitle, expectedTitle);	
		
	}
	
	@Before
	public void setUp() {
		
		System.out.println("Set up test");
		driver = DriverFactory.open("chrome");
		
	}
	@After
	public void tearDown() { 
		
		System.out.println("closing the test");
		driver.close();
	}
	/*@Test
	public void hungryTest() {
		
		boolean isHungry = true;
		assertTrue(isHungry);
	}*/
	
}
