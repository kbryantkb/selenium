package smoketest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.WebElement;

import utilities.DriverFactory;

public class ATagsTest {
  
	  
	private WebDriver driver;
		
		@Test
		public void loginElementsPresentTest() {
			System.out.println("Running Test");
			boolean createAccountPresent = false;
			List <WebElement> aElements = driver.findElements(By.tagName("a"));
			int numberOfAElements = aElements.size();
			System.out.println("There are " + numberOfAElements + " a tags on the page");
			for (WebElement aElement : aElements) {
				System.out.println(aElement.getText());
					if (aElement.getText().equals("CREATE ACCOUNT")) {
						createAccountPresent = true;
						break;
					}
			}
			Assert.assertTrue(createAccountPresent);
			
		}
		
		@BeforeMethod
		public void setUp() {
			System.out.println("Starting Test");
			driver = DriverFactory.open("chrome");
			String webURL = "http://sdettraining.com/trguitransactions/AccountManagement.aspx";
			driver.get(webURL);
		}
		
		@AfterMethod
		public void tearDown() {
			System.out.println("Closing Test");
			driver.close();
		}
	  
  }

