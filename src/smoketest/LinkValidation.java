package smoketest;


//import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utilities.CSV;
import utilities.DriverFactory;


class LinkValidation {

	WebDriver driver;
	
	// edit webURL to edit the web page being being scraped for links
	// edit userFile to edit the CSV being used to source links for comparison
	
	String webURL = "http://localhost:8888/email_templates/tulo_remake.html";
	String userFile = "/Users/keithbryant/Documents/tulo_links.csv";
	
	//variable list 
	List <WebElement> aElements;
	ArrayList <String> linkDocList = new ArrayList<String>();
	ArrayList <String> pageLinksList = new ArrayList<String>();
	ArrayList <String> resultsList = new ArrayList<String>();
	String hrefAttr;
	
	@Before
	public void setUp() {	
		System.out.println("hello world");
	}
	
	@Test
	public void test() {
		
		// CSV to Array block
		
		List <String[]> records = CSV.get(userFile);
		for (String[] record : records) {
			
			// the if statement is to take into account that the first row may just be headers
			// if the first row is not headers, then the if statement is not necessary
			
			if(records.indexOf(record) >= 1) {
				
				// this line is always necessary, and the record[i] indicates column number
				// remember that when parsing a CSV to an array that column numbers are O.B.O. so 1 actually indicates second column
				String linkConcat;
				String urlIgnore = record[2].toLowerCase();
				String recordIgnore = "ignore";
				if(urlIgnore.equals(recordIgnore)) {
					linkConcat = record[0] + "_" + urlIgnore;
					//linkDocList.add(linkConcat);
				}else {
					linkConcat = record[0] + "_" + record[1];
				}
				linkDocList.add(linkConcat);
				//linkDocList.add(record[1]);
			}
		}
		
		// System.out.println(linkDocList.get(2));
		
		// end CSV to Array block
		
		// scrape HTML for a href values and write them to Array
		
		driver = DriverFactory.open("chrome");
		driver.get(webURL);
	
		aElements = driver.findElements(By.tagName("a"));
		for (WebElement aElement : aElements) {
			hrefAttr = aElement.getAttribute("href");
			String QA_ID = aElement.getAttribute("QA_ID");
			String combAttr = QA_ID + "_" + hrefAttr;
			String urlIgnoreAttr = aElement.getAttribute("URL_ignore");
			String combIgnore = QA_ID + "_" + urlIgnoreAttr;
			String combIgnoreVal = "ignore";
			if(!Objects.isNull(QA_ID)) {
				if (Objects.equals(urlIgnoreAttr, combIgnoreVal)) {
					pageLinksList.add(combIgnore);
				}else {
					pageLinksList.add(combAttr);
				//pageLinksList.add(hrefAttr);
				}
			}
		}
		
		// System.out.println(pageLinksList.get(15));
		
		// end scrape HTML for a href values and write them to Array
		
		//comparison algorithm
		int i, j;
		String linkDocVal, pageLinkVal;
		for(i = 0; i < linkDocList.size(); i++) {
			linkDocVal = linkDocList.get(i);
			// System.out.println(linkDocVal);
			for(j = 0; j < pageLinksList.size(); j++) {
				pageLinkVal = pageLinksList.get(j);
				if(linkDocVal.equals(pageLinkVal)) {
					if (!resultsList.contains(linkDocVal))
						resultsList.add(linkDocVal);
						
				}
			}
		}
		
		// end comparison algorithm
		
		// print results Array and unit test
		System.out.println(linkDocList);
		System.out.println(pageLinksList);
		System.out.println(resultsList);
		Assert.assertEquals(linkDocList, resultsList);
		//driver.close();
	}
	
	@After
	public void tearDown() {
		//driver.close();
	}
	

}
