package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utilities.Excel;
import utilities.DriverFactory;

public class LoginDDT {
	
	WebDriver driver;
	WebElement emailAddress, userPassword, login;
	
	
	@Test(dataProvider = "getData")
	public void loginTest(String name, String email, String password) {
		//System.out.println("New Record " + name + " " + email + " " + password);
		
		defineElements();
		
		System.out.println("Running Test: " + name);
		emailAddress.sendKeys(email);
		userPassword.sendKeys(password);
		login.click();

	}
	
	public void defineElements() {
		emailAddress = driver.findElement(By.id("MainContent_txtUserName"));
		userPassword = driver.findElement(By.id("MainContent_txtPassword"));
		login = driver.findElement(By.id("MainContent_btnLogin"));
	}

	
	@BeforeMethod
	public void setUp() {
		
		driver = DriverFactory.open("chrome");
		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
		
	}
	
	@AfterMethod
	public void tearDown() {
		
		driver.quit();
		
	}
	
	@DataProvider
	public String[][] getData(){
		return Excel.get("/Users/keithbryant/Documents/UserLogin.xls");
	}

}
