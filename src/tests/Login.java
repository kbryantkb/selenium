package tests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import pages.DashboardPage;
import pages.LoginPageFactory;
import utilities.DriverFactory;
//import pages.LoginPage;

public class Login {
	
	// 1. initialize driver
		
	WebDriver driver;
	
	String username = "tim@testemail.com";
	String password = "trpass";
	
	@Test
	public void loginTestPOM() {
		driver = DriverFactory.open("chrome");
		
		driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
	
		LoginPageFactory loginPage = new LoginPageFactory(driver);
	// 2. enter login information (login page)
		loginPage.login(username, password);
	// 3. get confirmation (dash board page)
		DashboardPage dashboardPage = new DashboardPage(driver);
		
		String conf = dashboardPage.getConfirmation();
		System.out.println(conf);
		Assert.assertTrue(!conf.isEmpty());
	// 4. close driver
		
	driver.quit();
		
	}

}
