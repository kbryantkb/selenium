package tests;

import java.util.List;
import org.openqa.selenium.support.ui.Select;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utilities.DriverFactory;

@RunWith(value = Parameterized.class)
public class NewAccountDDT {
	
	private WebDriver driver;
	private String name, email, phone, gender, password, country;
	private boolean weeklyEmail, monthlyEmail, occasionalUpdate;
	private WebElement nameField, emailField, phoneField, passwordField, verifyPasswordField, countryField, maleButton, femaleButton, weeklyCheckbox, monthlyCheckbox, occasionalCheckbox, submitButton;
	
	public static String getConfirm(WebDriver drive) {
		WebElement confirmation = drive.findElement(By.id("MainContent_lblTransactionResult"));
		String output = confirmation.getText();
		return output;
	}
	
	@Test
	public void newAccountTest() {
		System.out.println("NEW RECORD " + name +" "+ email +" "+ phone);
		
		//setting web element variables
		
		defineWebElements();
		
		//fill out form
		
			//enter name, email and phone number
		
			nameField.sendKeys(name);
			emailField.sendKeys(email);
			phoneField.sendKeys(phone);
			
			//select gender
			
			if(gender.equals("male")){
				maleButton.click();
			}
			else {
				femaleButton.click();
			}
			
			//enter password 
			passwordField.sendKeys(password);
			verifyPasswordField.sendKeys(password);
			
			//select country
			
			new Select(countryField).selectByVisibleText(country);
			
			//checkbox logic
			
				//weekly checkbox
				
				if(weeklyEmail) {
					if(!weeklyCheckbox.isSelected()) {
						weeklyCheckbox.click();
					}
				}
				else {
					if(weeklyCheckbox.isSelected()) {
						weeklyCheckbox.click();
					}
				}
				
				//monthly checkbox
				
				if(monthlyEmail) {
					if(!monthlyCheckbox.isSelected()) {
						monthlyCheckbox.click();
					}
				}
				else {
					if(monthlyCheckbox.isSelected()) {
						monthlyCheckbox.click();
					}
				}
				
				//occasional checkbox
				
				if(occasionalUpdate) {
					if(!occasionalCheckbox.isSelected()) {
						occasionalCheckbox.click();
					}
				}
				else {
					if(occasionalCheckbox.isSelected()) {
						occasionalCheckbox.click();
					}
				}
				
			//end checkbox logic
			
		//end form entry
		
		//submit
		
		submitButton.click();
		
		//print confirmation to console or error message
		
		String expectedResults = "Customer information added successfully"; 
		
		Assert.assertEquals(expectedResults, getConfirm(driver));
		
	}
	
	@Before
	public void setUp() {
		//initializing WebDriver 

				driver = DriverFactory.open("chrome");

				// open sdet sign in page
				
				driver.get("http://sdettraining.com/trguitransactions/AccountManagement.aspx");
				
				//navigate to create account page
				
				driver.findElement(By.xpath("//*[@id='ctl01']/div[3]/div[2]/div/div[2]/a")).click();
	}
	
	@After
	public void tearDown() {
		driver.close();
	}
	
	public void defineWebElements() {
		//setting web element variables
		
				nameField = driver.findElement(By.id("MainContent_txtFirstName"));
				emailField = driver.findElement(By.id("MainContent_txtEmail"));
				phoneField = driver.findElement(By.id("MainContent_txtHomePhone"));
				passwordField = driver.findElement(By.id("MainContent_txtPassword"));
				verifyPasswordField = driver.findElement(By.id("MainContent_txtVerifyPassword"));
				countryField = driver.findElement(By.id("MainContent_menuCountry"));
				maleButton = driver.findElement(By.id("MainContent_Male"));
				femaleButton = driver.findElement(By.id("MainContent_Female"));
				weeklyCheckbox = driver.findElement(By.id("MainContent_checkWeeklyEmail"));
				monthlyCheckbox = driver.findElement(By.id("MainContent_checkMonthlyEmail"));
				occasionalCheckbox = driver.findElement(By.id("MainContent_checkUpdates"));
				submitButton = driver.findElement(By.id("MainContent_btnSubmit"));
	}
	
	@Parameters
	public static List <String[]> getData(){
		String accountCSV = "/Users/keithbryant/Documents/UserAccounts.csv";
		return utilities.CSV.get(accountCSV);
	}
	
	// constructor that passes parameters to the test method
	public NewAccountDDT(String name, String email, String phone, String gender, String password, String country, String weeklyEmail, String monthlyEmail, String occasionalUpdate) {
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.password = password;
		this.country = country;
		if (weeklyEmail.equals("TRUE")) {
			this.weeklyEmail = true;
		}
		else if(weeklyEmail.equals("FALSE")) {
			this.weeklyEmail = false;
		}
		if (monthlyEmail.equals("TRUE")) {
			this.monthlyEmail = true;
		}
		else if(monthlyEmail.equals("FALSE")) {
			this.monthlyEmail = false;
		}
		if (occasionalUpdate.equals("TRUE")) {
			this.occasionalUpdate = true;
		}
		else if(occasionalUpdate.equals("FALSE")) {
			this.occasionalUpdate = false;
		}
		
	}

}
